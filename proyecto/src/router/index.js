import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeSer from '../views/PortalSer.vue'
import Portada from '../components/PaginaPrincipal/Portada.vue'
import Contacto from '../components/PaginaPrincipal/Contacto.vue'
import Ser from '../components/PaginaPrincipal/Ser.vue'
import CIAD from '../views/Home.vue'
import PortalBusquedas from '../components/CIAD/PortalBusquedas.vue'
import PortalUsuarios from '../components/CIAD/PortalUsuarios.vue'
import Enlaces from '../components/CIAD/Enlaces.vue'
import Login from '../views/Login.vue'
import Dashboard from '../views/Dashboard.vue'
import Tematica from '../components/Tematicas/Tematica.vue'
import Users from '../components/Usuarios/Usuarios.vue'
import Books from '../components/Libros/Libros.vue'


import Downloads from '../components/Descargas/Descargas.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomeSer',
    component: HomeSer,
    children:[
      {
        path: '/',
        name: 'portada',
        component: Portada,       
      },
      {
        path: 'contacto',
        name: 'contacto',
        component: Contacto,       
      },
      {
        path: 'ser',
        name: 'ser',
        component: Ser,       
      },
    ]
  },
  {
    path: '/ciad',
    name: 'CIAD',
    component: CIAD,
    children:[
      {
        path: '/',
        name: 'PortalUsuarios',
        component: PortalUsuarios
      },
      {
        path: '/busqueda',
        name: 'Buscar',
        component: PortalBusquedas
      },
      {
        path: '/enlaces',
        name: 'enlaces',
        component: Enlaces
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    children:[
      {
        path: '',
        name: 'Books',
        component: Books,
        meta: {auth: true}        
      },
      {
        path: '/tematica',
        name: 'Tematica',
        component: Tematica,
        meta: {auth: true}
      },
      {
        path: '/usuarios',
        name: 'Users',
        component: Users,
        meta: {auth: true}
      },
      {
        path: '/books',
        name: 'Books',
        component: Books,
        meta: {auth: true}
      },
      {
        path: '/downloads',
        name: 'Descargas',
        component: Downloads,
        meta: {auth: true}
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.auth) {
    const token = sessionStorage.getItem('token')
    if (token) {
      next()
    } else {
      next({ name: 'auth' })
    }
  } else {
    next()
  }
})

export default router
