import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    tematicas: [],
    categoria: sessionStorage.getItem('categoria') || '',
  },
  mutations: {
    setTematica (state, value) {
      state.tematicas = value
    },
    setCategoria (state, value) {
      sessionStorage.setItem('categoria', value)
      state.categoria = value
      console.log(value)
    }
  },
  getters: {
    getTematicas (state) {
      return state.tematicas
    },
    getCategoria (state) {
      return state.categoria
    }
  },
  actions: {
  },
  modules: {
  }
})
