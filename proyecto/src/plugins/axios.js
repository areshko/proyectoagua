import axios from 'axios'
import { host } from '../config'

const auth = axios.create({ baseURL: `${host}/auth` })
const tematicas = axios.create({ baseURL: `${host}/tematicas` })
const users = axios.create({ baseURL: `${host}/users` })
const books = axios.create({ baseURL: `${host}/books` })
const descargases = axios.create({ baseURL: `${host}/descargases` })

const services = {
    $http: {
        get () {
            return {
                auth,
                tematicas,
                users,
                books,
                descargases
            }
        },
        post() {
            return {
                auth,
                tematicas,
                users,
                books,
                descargases
            }
        },
        put() {
            return {
                auth,
                tematicas,
                users,
                books
            }
        }
    }

}

export default (Vue) => {
    Object.defineProperties(Vue.prototype, services)
  }